$(document).ready(function() {
   resizer();
   
   $(window).resize(function(){
      resizer();
   });

   $('.one-swicth-slide').click(function(){
      var sl = $(this).closest('.slider1');

      sl.find('.one-swicth-slide').removeClass("active");
      $(this).addClass("active");

      sl.css({"background-image":"url('"+$(this).attr('bg')+"')"});
   });
   
   setInterval(function(){
      var ac = $('.slider1 .one-swicth-slide.active'),
         n = ac.prev('.one-swicth-slide').eq(0);
      if (n.length===0){
         n = $('.slider1 .one-swicth-slide').eq($('.slider1 .one-swicth-slide').length-1);
      }
      n.trigger("click");
   },5000);
   
   
   

   $('.one-preview').click(function(){
      var sl = $(this).closest('.slider-big-img');

      sl.find('.one-preview').removeClass("active");
      $(this).addClass("active");

      sl.css({"background-image":"url('"+$(this).attr('bg')+"')"});
   });


   $('.check').click(function(){
      $(this).toggleClass("active");
      $(this).find('input').val($(this).hasClass("active"));
   });

   


   $('.one-blue-switch').click(function(){
      var sl = $(this).closest('.quote');

      sl.find('.one-blue-switch').removeClass("active");
      $(this).addClass("active");

      sl.find('.photo-quote').removeClass('active');
      sl.find('.right-quote').removeClass('active');
      sl.find('[group="'+$(this).attr('gr')+'"]').addClass('active');

   });


   $('.input-with-def, .search-str').focus(function(){
      var def = $(this).attr("def"),
         val  = $(this).val();

      if (def===val){
         $(this).val("");
      }
   });

   $('.input-with-def, .search-str').blur(function(){
      var def = $(this).attr("def"),
         val  = $(this).val();

      if (""===val){
         $(this).val(def);
      }
   });

   $('.input-with-def').keydown(function(ev){

      if (ev.keyCode!==9 && (ev.keyCode<48 || ev.keyCode>57))
         return false;
   });

   try{
      setTimeout(function(){calcTab();}, 1000);
   }catch(e){}

   $('.one-tab-button').click(function(){

      $('.one-tab-button').removeClass("active");
      $(this).addClass("active");

      $('.tab-content').removeClass("active");
      $('#'+$(this).attr("tab")).addClass("active");

      calcTab();
   });


});

function validate(address) {
   var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
   if(reg.test(address) == false) {
      alert('Введите корректный e-mail');
      return false;
   }
}

function resizer(){
   var wsl = 172,
      bw = $('body').width(),
      colIco = $('.one-ico').length,
      col = parseInt(colIco/($('body').width()/wsl),10)+1,
      allI = $('.one-ico'),
      inCol = colIco/col;
   
   allI.appendTo($('.all-ico'));
   $('.slide').remove();
   $('.one-swicth-slide2').remove();
   
   var ac = null,
      curr = 1;
   
   for(var k=0;k<colIco;k++){
      if (!ac){
         $('<div class="one-swicth-slide2" slide="sl'+curr+'"></div>').appendTo($('.switch-slide2'));
         ac = $('<div class="slide" id="sl'+curr+'"><div></div></div>').appendTo($('.all-slides'));
      }
      allI.eq(k).appendTo(ac);
      if ((k+1)>=curr*inCol){
         curr++;
         ac = null;
      }
      
   }
   
   $('.one-swicth-slide2').eq(0).addClass("active");
   $('.slide').eq(0).addClass("active");
   
   
   $('.one-swicth-slide2').click(function(){

      var sl = $(this).closest('.slider2');

      sl.find('.one-swicth-slide2').removeClass("active");
      $(this).addClass("active");

      sl.find('.slide').removeClass('active');
      sl.find('#'+$(this).attr('slide')).addClass('active');
   });
   
}

function calcTab(){
   var ac = $('.one-tab-button.active'),
      left = ac.offset().left,
      bw = $('body').width(),
      w = ac.width();

   var cw = bw - left - w - 280;

   $('.right1').css("width", cw+"px");
   $('.left1').css("width", (left-200)+"px");
}